namespace :coupons do
  require "nokogiri"
  desc "Grab coupons data"
  task data_load: :environment do
    url_ninecoupons = 'http://api.8coupons.com/v1/getrealtimelocaldeals?key=32e69a72d51df32654572b89a5378f8d6f31f7e5576f5302cf1e33b7edd1cc19b5bca31b81fb4ffe85f5bb02fad941b3&userid=18381'
    url_gdeslon = 'http://www.gdeslon.ru/api/coupons.xml?search%5Bcategory_ids%5D%5B%5D=139&search%5Bcategory_ids%5D%5B%5D=196&search%5Bcategory_ids%5D%5B%5D=227&search%5Bcategory_ids%5D%5B%5D=495&search%5Bcategory_ids%5D%5B%5D=164&search%5Bcategory_ids%5D%5B%5D=479&search%5Bcategory_ids%5D%5B%5D=234&search%5Bkind%5D=4&search%5Bcoupon_type%5D%5B%5D=coupons&search%5Bcoupon_type%5D%5B%5D=promo&api_token=44da508c23c400fce99f516127ee2790698e7b7b'
    xml_data = Net::HTTP.get_response(URI.parse(url_gdeslon)).body
    json_data = Net::HTTP.get_response(URI.parse(url_ninecoupons)).body
    doc = Nokogiri::XML(xml_data)
    doc.search('//kinds/kind').each do |kind|
      db = GdeslonKinds.new
      db.name = kind.at('name').text
      db.id = kind.at('id').text
      db.save
    end
    doc.search('//categories/category').each do |category|
      db = GdeslonCategories.new
      #p category.at("name").text
      db.name = category.xpath('name').text
      db.id = category.xpath('id').text
      db.save
    end
    doc.search('//merchants/merchant').each do |merchant|
      db = GdeslonMerchant.new
      db.name = merchant.xpath('name').text
      db.id = merchant.xpath('id').text #FIXME неправильно приcвавается id
      db.save
    end
    doc.search('//coupon-categories/coupon-category').each do |coupon_category|
      db = GdeslonCouponCategories.new
      db.name = coupon_category.xpath('name').text
      db.id = coupon_category.xpath('id').text
      db.save
    end
    doc.search('//coupons/coupon').each do |coupon|
      db = GdeslonCoupon.new
      db.name = coupon.xpath('name').text
      db.coupon_id = coupon.xpath('id').text
      db.description = coupon.xpath('description').text
      db.gdeslon_merchant_id = coupon.xpath('merchant-id').text
      db.code = coupon.xpath('code').text
      db.date_end = coupon.xpath('finish-at').text
      db.date_start = coupon.xpath('start-at').text
      db.url = coupon.xpath('url-with-code').text
      db.gdeslon_kind_id = coupon.xpath('kind').text
      db.save
    end
    json_doc = JSON.parse(json_data)
    json_doc.each do |coupon|
      db = NineCouponCoupon.new
      db.name = coupon['name']
      db.address = coupon['address']
      db.city = coupon['city']
      db.store_id = coupon['storeID']
      db.description = coupon['dealinfo']
      db.coupon_id = coupon['ID']
      db.url = coupon['URL']
      db.date_end = coupon['expirationDate']
      db.date_start = coupon['postDate']
      db.save

    end
  end
end
