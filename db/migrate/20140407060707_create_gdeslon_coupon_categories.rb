class CreateGdeslonCouponCategories < ActiveRecord::Migration
  def change
    create_table :gdeslon_coupon_categories, :id => false do |t|
      t.string :name, nill: false
      t.integer :id,  null: false, default: 0
    end
  end
end
