class CreateNineCouponCoupons < ActiveRecord::Migration
  def change
    create_table :nine_coupon_coupons do |t|
      t.string :name, null: false, default: 'None'
      t.string :address, default: 'None'
      t.string :city, default: 'None'
      t.integer :store_id, null: false
      t.string :description, default: 'None'
      t.integer :coupon_id, null: false, default: 0
      t.string :url, null: false, default: 'None'
      t.datetime :date_end, null: false, default: 0
      t.datetime :date_start, null: false, default: 0
      t.timestamps
    end
  end
end
