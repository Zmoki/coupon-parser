class CreateGdeslonCoupons < ActiveRecord::Migration
  def change
    create_table :gdeslon_coupons do |t|
      t.string :name, null: false, default: 'None'
      t.string :code, default: 'None'
      t.datetime :date_end, null: false, default: 0
      t.datetime :date_start, null: false, default: 0
      t.string :description, default: 'None'
      t.integer :coupon_id, null: false, default: 0
      t.string :url, null: false, default: 'None'
      t.belongs_to :gdeslon_kind
      t.belongs_to :gdeslon_merchant
      t.timestamps
    end
  end
end
