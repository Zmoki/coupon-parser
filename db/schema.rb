# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140409151515) do

  create_table "gdeslon_categories", id: false, force: true do |t|
    t.string  "name"
    t.integer "id",   default: 0, null: false
  end

  create_table "gdeslon_coupon_categories", id: false, force: true do |t|
    t.string  "name"
    t.integer "id",   default: 0, null: false
  end

  create_table "gdeslon_coupons", force: true do |t|
    t.string   "name",                default: "None", null: false
    t.string   "code",                default: "None"
    t.datetime "date_end",                             null: false
    t.datetime "date_start",                           null: false
    t.string   "description",         default: "None"
    t.integer  "coupon_id",           default: 0,      null: false
    t.string   "url",                 default: "None", null: false
    t.integer  "gdeslon_kind_id"
    t.integer  "gdeslon_merchant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "gdeslon_kinds", id: false, force: true do |t|
    t.string  "name"
    t.integer "id",   default: 0, null: false
  end

  create_table "gdeslon_merchants", id: false, force: true do |t|
    t.string  "name"
    t.integer "id",   default: 0, null: false
  end

  create_table "nine_coupon_coupons", force: true do |t|
    t.string   "name",        default: "None", null: false
    t.string   "address",     default: "None"
    t.string   "city",        default: "None"
    t.integer  "store_id",                     null: false
    t.string   "description", default: "None"
    t.integer  "coupon_id",   default: 0,      null: false
    t.string   "url",         default: "None", null: false
    t.datetime "date_end",                     null: false
    t.datetime "date_start",                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
