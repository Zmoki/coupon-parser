class CouponsController < ApplicationController

  def index
   @coupons = GdeslonCoupon.all
   @coupons = GdeslonCoupon.page(params[:page]) if params[:db] === 'gdeslon'
   @coupons = NineCouponCoupon.page(params[:page]) if params[:db] === '8coupons'
  end

  def gdeslon_show
    render_404 unless @coupon = GdeslonCoupon.find_by_id(params[:id])
  end

  def gdeslon_destroy
    @coupon = GdeslonCoupon.find_by_id(params[:id])
    @coupon.destroy
  end

  def ninecoupons_show
     render_404 unless @coupon = NineCouponCoupon.find_by_id(params[:id])
  end

  def ninecoupons_destroy
    @coupon = NineCouponCoupon.find_by_id(params[:id])
    @coupon.destroy
  end
  private

  def render_404
    raise ActionController::RoutingError.new('Not Found')
  end
end
