
$(window).load( function() {
    jQuery(function(s){
      $('.delete_action').bind( "click", function () {
        var current_coupon_parent = $(this).parents('tr')[0];
        var coupon_id = $(current_coupon_parent).attr('class').split(".")[1]
        var coupon_base = $(current_coupon_parent).attr('class').split(".")[0]
        $.ajax({
            url:'/coupons/' + coupon_base + '/' + coupon_id,
            type: 'POST',
            data: {_method: 'DELETE'},
            success: function() {
              $(current_coupon_parent).fadeOut(200);}
          });
       });
    });
});

