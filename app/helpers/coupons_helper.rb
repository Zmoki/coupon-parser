module CouponsHelper

  def get_kind(s)
    kind = GdeslonKinds.find_by_id(s)
    kind.name
  end

  def get_merchant(s)
    merchant = GdeslonMerchant.find_by_id(s)
    merchant.name if merchant
  end


end
