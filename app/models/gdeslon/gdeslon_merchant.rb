class GdeslonMerchant < ActiveRecord::Base
  has_many :gdeslon_coupons
  has_many :gdeslon_categories
  validates_uniqueness_of :id
end
