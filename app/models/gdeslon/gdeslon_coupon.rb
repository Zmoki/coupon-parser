class GdeslonCoupon < ActiveRecord::Base
  has_one :gdeslon_merchant
  has_one :gdeslon_kind
  has_many :gdeslon_coupon_categories
  validates_uniqueness_of :coupon_id


end
